package org.petrpan.ebookcatalog;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.petrpan.ebookcatalog.controllers.MainController;
import org.petrpan.ebookcatalog.model.*;

public class Main extends Application {
    public static final Image MAIN_ICON = new Image("/icon.png");

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        DBDriver.getInstance().initDriver();
        var loader = new FXMLLoader(getClass().getResource("/main.fxml"));
        Parent root = loader.load();
        ((MainController) loader.getController()).setPrimaryStage(stage);
        stage.setTitle("eBooks Catalog");
        Scene scene = new Scene(root, 1280, 800);
        stage.setScene(scene);
        stage.getIcons().add(MAIN_ICON);
        stage.setMaximized(true);
        stage.show();
    }
}
