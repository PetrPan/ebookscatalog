package org.petrpan.ebookcatalog.controllers;

import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.petrpan.ebookcatalog.Main;
import org.petrpan.ebookcatalog.model.Author;
import org.petrpan.ebookcatalog.model.AuthorsList;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class AuthorsController extends UpdateableController implements Initializable {
    private TreeItem<Author> root = new TreeItem<>(new Author("Все авторы"));
    public TreeTableView<Author> authorsTreeTableView;

    private Optional<String> showDialog(String author, String title) {
        Dialog<String> dialog = new TextInputDialog(author);
        ((Stage)dialog.getDialogPane().getScene().getWindow()).getIcons().add(Main.MAIN_ICON);
        dialog.getDialogPane().getButtonTypes().setAll(ButtonType.OK, new ButtonType("Отмена", ButtonBar.ButtonData.CANCEL_CLOSE));
        dialog.setHeaderText(null);
        dialog.setGraphic(null);
        dialog.setTitle("Переименовать");
        return dialog.showAndWait();
    }

    private ContextMenu createContextMenu() {
        MenuItem addMenuItem = new MenuItem("Добавить автора");
        addMenuItem.setOnAction(actionEvent -> {
            Optional<String> optional = showDialog(null, "Новый автор");
            optional.ifPresent(s -> {
                var newAuthor = new Author(s);
                newAuthor.saveToDB();
                var treeItem = new TreeItem<>(newAuthor);
                root.getChildren().add(treeItem);
                authorsTreeTableView.getSelectionModel().select(treeItem);
                update();
                if (onChangeCallback != null) onChangeCallback.changeNotification();
            });
        });

        MenuItem changeMenuItem = new MenuItem("Переименовать");
        changeMenuItem.setOnAction(actionEvent -> {
            var treeItem = (TreeItem<Author>) authorsTreeTableView.getSelectionModel().getSelectedItem();
            if (treeItem == null || treeItem == root) return;
            Optional<String> optional = showDialog(treeItem.getValue().getName(), "Переименовать");
            optional.ifPresent(s -> {
                treeItem.getValue().setName(s);
                treeItem.getValue().saveToDB();
                update();
                if (onChangeCallback != null) onChangeCallback.changeNotification();
            });
        });

        MenuItem removeMenuItem = new MenuItem("Удалить");
        removeMenuItem.setOnAction(actionEvent -> {
            var treeItem = (TreeItem<Author>) authorsTreeTableView.getSelectionModel().getSelectedItem();
            if (treeItem == null || treeItem == root) return;
            Alert alert = new Alert(Alert.AlertType.NONE);
            ((Stage)alert.getDialogPane().getScene().getWindow()).getIcons().add(Main.MAIN_ICON);
            alert.getDialogPane().getButtonTypes().setAll(ButtonType.OK, new ButtonType("Отмена", ButtonBar.ButtonData.CANCEL_CLOSE));
            alert.setTitle("Удаление");
            alert.setContentText("Удалить автора «" + treeItem.getValue().getName() + "» ?");
            Optional<ButtonType> result = alert.showAndWait();
            result.ifPresent(buttonType -> {
                if (buttonType == ButtonType.OK) {
                    treeItem.getValue().deleteFromDB();
                    root.getChildren().remove(treeItem);
                    if (onChangeCallback != null) onChangeCallback.changeNotification();
                }
            });
        });

        return new ContextMenu(addMenuItem, changeMenuItem, removeMenuItem);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Context.getInstance().setController(authorsTreeTableView, this);
        TreeTableColumn<Author, String> authorNameColumn = new TreeTableColumn<>("Автор");
        authorNameColumn.setCellValueFactory(p -> p.getValue().getValue().nameProperty());

        authorsTreeTableView.getColumns().add(authorNameColumn);
        authorsTreeTableView.setContextMenu(createContextMenu());

        root.setExpanded(true);
        authorsTreeTableView.setRoot(root);
        authorsTreeTableView.setSortMode(TreeSortMode.ALL_DESCENDANTS);

        update();
    }

    @Override
    public void update() {
        var selectedItem = authorsTreeTableView.getSelectionModel().getSelectedItem();
        Author selectedAuthor = selectedItem == null ? null : selectedItem.getValue();
        root.getChildren().clear();
        for (Author author : AuthorsList.getInstance().readFromDB()) {
            var treeItem = new TreeItem<>(author);
            root.getChildren().add(treeItem);
            if (selectedAuthor != null && selectedAuthor.equals(author))
                authorsTreeTableView.getSelectionModel().select(treeItem);
        }
        if (authorsTreeTableView.getSelectionModel().getSelectedItem() == null) {
            authorsTreeTableView.getSelectionModel().select(root);
        }
    }
}
