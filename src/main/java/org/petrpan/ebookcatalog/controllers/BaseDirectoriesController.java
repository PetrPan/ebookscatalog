package org.petrpan.ebookcatalog.controllers;

import javafx.application.Platform;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import org.petrpan.ebookcatalog.Main;
import org.petrpan.ebookcatalog.model.BaseDirectoriesList;
import org.petrpan.ebookcatalog.model.BaseDirectory;
import org.petrpan.ebookcatalog.model.FilesList;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class BaseDirectoriesController extends UpdateableController implements Initializable {
    public TreeTableView<BaseDirectory> baseDirectoiesTreeTableView;
    private TreeItem<BaseDirectory> root = new TreeItem<>(new BaseDirectory("Все директории"));
    private DirectoryChooser directoryChooser = new DirectoryChooser();

    private Optional<String> showDirectorySelectDialog(String directoryPath, String title) {
        Dialog<String> dialog = new Dialog<>();
        dialog.setTitle(title);
        ((Stage)dialog.getDialogPane().getScene().getWindow()).getIcons().add(Main.MAIN_ICON);
        ButtonType okButtonType = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(okButtonType, new ButtonType("Отмена", ButtonBar.ButtonData.CANCEL_CLOSE));

        TextField path = new TextField();
        path.setText(directoryPath);
        path.setPromptText("Путь к каталогу...");
        path.setPrefWidth(300);

        Button button = new Button("...");
        button.setOnAction(actionEvent -> {
            java.io.File dir = directoryChooser.showDialog(dialog.getDialogPane().getScene().getWindow());
            if (dir != null) {
                path.setText(dir.getAbsolutePath());
            } else {
                path.setText(null);
            }
        });

        HBox hBox = new HBox(path, button);
        hBox.setSpacing(3);
        dialog.getDialogPane().setContent(hBox);

        Platform.runLater(path::requestFocus);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == okButtonType && !path.getText().isEmpty()) {
                return path.getText();
            }
            return null;
        });

        return dialog.showAndWait();
    }

    private ContextMenu createContextMenu() {
        MenuItem addMenuItem = new MenuItem("Добавить");
        addMenuItem.setOnAction(actionEvent -> {
            Optional<String> optional = showDirectorySelectDialog(null, "Новая директория");
            optional.ifPresent(s -> {
                var newBaseDirectory = new BaseDirectory(s);
                newBaseDirectory.saveToDB();
                var treeItem = new TreeItem<>(newBaseDirectory);
                root.getChildren().add(treeItem);
                baseDirectoiesTreeTableView.getSelectionModel().select(treeItem);
                update();
                if (onChangeCallback != null) onChangeCallback.changeNotification();
            });
        });

        MenuItem changeMenuItem = new MenuItem("Переименовать");
        changeMenuItem.setOnAction(actionEvent -> {
            var treeItem = (TreeItem<BaseDirectory>) baseDirectoiesTreeTableView.getSelectionModel().getSelectedItem();
            if (treeItem == null || treeItem == root) return;
            Optional<String> optional = showDirectorySelectDialog(treeItem.getValue().getPath(), "Переименовать");
            optional.ifPresent(s -> {
                treeItem.getValue().setPath(s);
                treeItem.getValue().saveToDB();
                update();
                if (onChangeCallback != null) onChangeCallback.changeNotification();
            });
        });

        MenuItem removeMenuItem = new MenuItem("Удалить");
        removeMenuItem.setOnAction(actionEvent -> {
            var treeItem = (TreeItem<BaseDirectory>) baseDirectoiesTreeTableView.getSelectionModel().getSelectedItem();
            if (treeItem == null || treeItem == root) return;
            Alert alert = new Alert(Alert.AlertType.NONE);
            ((Stage)alert.getDialogPane().getScene().getWindow()).getIcons().add(Main.MAIN_ICON);
            alert.getDialogPane().getButtonTypes().setAll(ButtonType.OK, new ButtonType("Отмена", ButtonBar.ButtonData.CANCEL_CLOSE));
            alert.setTitle("Удаление");
            alert.setContentText("Удалить из каталога директорию\n «" + treeItem.getValue().getPath() + "» ?");
            Optional<ButtonType> result = alert.showAndWait();
            result.ifPresent(buttonType -> {
                if (buttonType == ButtonType.OK) {
                    treeItem.getValue().deleteFromDB();
                    root.getChildren().remove(treeItem);
                    if (onChangeCallback != null) onChangeCallback.changeNotification();
                }
            });
        });

        MenuItem updateMenuItem = new MenuItem("Перечитать");
        updateMenuItem.setOnAction(actionEvent -> {
            var treeItem = (TreeItem<BaseDirectory>) baseDirectoiesTreeTableView.getSelectionModel().getSelectedItem();
            if (treeItem == null || treeItem == root) return;
            try {
                FilesList.getInstance().readFromDisc(treeItem.getValue());
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (onChangeCallback != null) onChangeCallback.changeNotification();
        });

        return new ContextMenu(updateMenuItem, new SeparatorMenuItem(), addMenuItem, changeMenuItem, removeMenuItem);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Context.getInstance().setController(baseDirectoiesTreeTableView, this);
        TreeTableColumn<BaseDirectory, String> baseDirectoryPathColumn = new TreeTableColumn<>("Путь");
        baseDirectoryPathColumn.setCellValueFactory(p -> p.getValue().getValue().pathProperty());

        baseDirectoiesTreeTableView.getColumns().addAll(List.of(baseDirectoryPathColumn));
        baseDirectoiesTreeTableView.setContextMenu(createContextMenu());

        root.setExpanded(true);
        baseDirectoiesTreeTableView.setRoot(root);

        update();
    }

    @Override
    void update() {
        var selectedItem = baseDirectoiesTreeTableView.getSelectionModel().getSelectedItem();
        BaseDirectory selectedBaseDirectory = selectedItem == null ? null : selectedItem.getValue();
        root.getChildren().clear();

        for (BaseDirectory baseDirectory : BaseDirectoriesList.getInstance().readFromDB()) {
            var treeItem = new TreeItem<>(baseDirectory);
            root.getChildren().add(treeItem);
            if (selectedBaseDirectory != null && selectedBaseDirectory.equals(baseDirectory))
                baseDirectoiesTreeTableView.getSelectionModel().select(treeItem);
        }
        if (baseDirectoiesTreeTableView.getSelectionModel().getSelectedItem() == null) {
            baseDirectoiesTreeTableView.getSelectionModel().select(root);
        }
    }
}
