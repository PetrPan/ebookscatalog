package org.petrpan.ebookcatalog.controllers;

import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.stage.Stage;
import org.petrpan.ebookcatalog.Main;
import org.petrpan.ebookcatalog.model.CategoriesList;
import org.petrpan.ebookcatalog.model.Category;
import org.petrpan.ebookcatalog.model.DBDriver;

import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

public class CategoriesController extends UpdateableController implements Initializable {
    public TreeTableView<Category> categoriesTreeTableView;
    private TreeItem<Category> root = new TreeItem<>(new Category(0, 0, "Все категории"));
    private Map<Long, TreeItem<Category>> indexOfTree = new LinkedHashMap<>();

    private void newCategory(TreeItem<Category> parentItem) {
        Dialog<String> dialog = new TextInputDialog();
        ((Stage) dialog.getDialogPane().getScene().getWindow()).getIcons().add(Main.MAIN_ICON);
        dialog.getDialogPane().getButtonTypes().setAll(ButtonType.OK, new ButtonType("Отмена", ButtonBar.ButtonData.CANCEL_CLOSE));
        dialog.setHeaderText(null);
        dialog.setGraphic(null);
        dialog.setTitle("Новая категория");
        Optional<String> optional = dialog.showAndWait();
        optional.ifPresent(s -> {
            var newCategory = new Category(parentItem.getValue().getId(), parentItem.getValue().getLevel() + 1, s);
            newCategory.saveToDB();
            var treeItem = new TreeItem<>(newCategory);
            parentItem.getChildren().add(treeItem);
            parentItem.setExpanded(true);
            categoriesTreeTableView.getSelectionModel().select(treeItem);
            update();
            if (onChangeCallback != null) onChangeCallback.changeNotification();
        });
    }

    private void setLevel(TreeItem<Category> treeItem, int newLevel, Connection connection) {
        if (treeItem.getValue().getLevel() == newLevel) return;
        treeItem.getValue().setLevel(newLevel);
        for (TreeItem<Category> child : treeItem.getChildren()) {
            setLevel(child, newLevel + 1, connection);
            child.getValue().saveToDB(connection);
        }
        treeItem.getValue().saveToDB(connection);
    }

    private ContextMenu createContextMenu() {
        MenuItem addMenuItem = new MenuItem("Добавить категорию");
        addMenuItem.setOnAction(actionEvent -> {
            var treeItem = (TreeItem<Category>) categoriesTreeTableView.getSelectionModel().getSelectedItem();
            newCategory(treeItem == null || treeItem == root ? root : treeItem.getParent());
        });

        MenuItem addSubMenuItem = new MenuItem("Добавить подчиненную категорию");
        addSubMenuItem.setOnAction(actionEvent -> {
            var treeItem = (TreeItem<Category>) categoriesTreeTableView.getSelectionModel().getSelectedItem();
            newCategory(treeItem == null ? root : treeItem);
        });

        MenuItem changeMenuItem = new MenuItem("Переименовать");
        changeMenuItem.setOnAction(actionEvent -> {
            var treeItem = (TreeItem<Category>) categoriesTreeTableView.getSelectionModel().getSelectedItem();
            if (treeItem == null || treeItem == root) return;
            Dialog<String> dialog = new TextInputDialog(treeItem.getValue().getName());
            ((Stage) dialog.getDialogPane().getScene().getWindow()).getIcons().add(Main.MAIN_ICON);
            dialog.getDialogPane().getButtonTypes().setAll(ButtonType.OK, new ButtonType("Отмена", ButtonBar.ButtonData.CANCEL_CLOSE));
            dialog.setHeaderText(null);
            dialog.setGraphic(null);
            dialog.setTitle("Переименовать");
            Optional<String> optional = dialog.showAndWait();
            optional.ifPresent(s -> {
                treeItem.getValue().setName(s);
                treeItem.getValue().saveToDB();
                update();
                if (onChangeCallback != null) onChangeCallback.changeNotification();
            });
        });

        MenuItem removeMenuItem = new MenuItem("Удалить");
        removeMenuItem.setOnAction(actionEvent -> {
            var treeItem = (TreeItem<Category>) categoriesTreeTableView.getSelectionModel().getSelectedItem();
            if (treeItem == null || treeItem == root) return;
            if (!treeItem.getChildren().isEmpty()) {
                Alert alert = new Alert(Alert.AlertType.NONE, "Сначала необходимо удалить подчиненные элементы!", ButtonType.OK);
                ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(Main.MAIN_ICON);
                alert.show();
                return;
            }
            Alert alert = new Alert(Alert.AlertType.NONE);
            ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(Main.MAIN_ICON);
            alert.getDialogPane().getButtonTypes().setAll(ButtonType.OK, new ButtonType("Отмена", ButtonBar.ButtonData.CANCEL_CLOSE));
            alert.setTitle("Удаление");
            alert.setContentText("Удалить категорию «" + treeItem.getValue().getName() + "» ?");
            Optional<ButtonType> result = alert.showAndWait();
            result.ifPresent(buttonType -> {
                if (buttonType == ButtonType.OK) {
                    treeItem.getValue().deleteFromDB();
                    treeItem.getParent().getChildren().remove(treeItem);
                    if (onChangeCallback != null) onChangeCallback.changeNotification();
                }
            });
        });

        return new ContextMenu(addMenuItem, addSubMenuItem, changeMenuItem, removeMenuItem);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Context.getInstance().setController(categoriesTreeTableView, this);
        TreeTableColumn<Category, String> categoryNameColumn = new TreeTableColumn<>("Категория");
        categoryNameColumn.setCellValueFactory(p -> p.getValue().getValue().nameProperty());
        categoriesTreeTableView.getColumns().add(categoryNameColumn);
        categoriesTreeTableView.setContextMenu(createContextMenu());

        categoriesTreeTableView.setRowFactory(param -> {
            final TreeTableRow<Category> row = new TreeTableRow<>();
            row.setOnDragDetected(event -> {
                int focusedIndex = categoriesTreeTableView.getSelectionModel().getFocusedIndex();
                if (focusedIndex > 0) {
                    Dragboard db = categoriesTreeTableView.startDragAndDrop(TransferMode.ANY);
                    db.setDragView(row.snapshot(null, null));
                    ClipboardContent content = new ClipboardContent();
                    content.putString(Integer.toString(focusedIndex));
                    db.setContent(content);
                    event.consume();
                }
            });
            row.setOnDragOver(event -> {
                if (event.getDragboard().hasString()) event.acceptTransferModes(TransferMode.MOVE);
                event.consume();
            });
            row.setOnDragDropped(event -> {
                boolean success = false;
                if (event.getDragboard().hasString()) {
                    TreeItem<Category> source = categoriesTreeTableView.getTreeItem(Integer.parseInt(event.getDragboard().getString()));
                    TreeItem<Category> destination = row.isEmpty() ? categoriesTreeTableView.getRoot() : row.getTreeItem();
                    boolean canMove = true;
                    for (TreeItem<Category> parent = destination.getParent(); parent != null; parent = parent.getParent()) {
                        if (parent.getValue().getId() == source.getValue().getId()) {
                            canMove = false;
                            break;
                        }
                    }
                    if (canMove) {
                        source.getValue().setParent(destination.getValue().getId());
                        source.getParent().getChildren().remove(source);
                        destination.getChildren().add(source);
                        destination.setExpanded(true);
                        try (Connection connection = DBDriver.getInstance().getConnection()){
                            connection.setAutoCommit(false);
                            setLevel(source, destination.getValue().getLevel() + 1, connection);
                            connection.commit();
                        } catch (SQLException e) {
                            throw new RuntimeException(e);
                        }
                        categoriesTreeTableView.getSelectionModel().select(source);
                        update();
                        if (onChangeCallback != null) onChangeCallback.changeNotification();
                        success = true;
                    }
                }
                event.setDropCompleted(success);
                event.consume();
            });
            return row;
        });

        root.setExpanded(true);
        categoriesTreeTableView.setRoot(root);

        update();
    }

    @Override
    void update() {
        var selectedItem = categoriesTreeTableView.getSelectionModel().getSelectedItem();
        Category selectedCategory = selectedItem == null ? null : selectedItem.getValue();
        root.getChildren().clear();

        indexOfTree.clear();
        indexOfTree.put(0L, root);

        for (Category category : CategoriesList.getInstance().readFromDB()) {
            var categoryTreeItem = new TreeItem<>(category);
            var parentTreeItem = indexOfTree.get(category.getParent());
            parentTreeItem.getChildren().add(categoryTreeItem);
            if (!parentTreeItem.isExpanded()) parentTreeItem.setExpanded(true);
            indexOfTree.put(category.getId(), categoryTreeItem);
            if (selectedCategory != null && selectedCategory.equals(category)) {
                categoriesTreeTableView.getSelectionModel().select(categoryTreeItem);
            }
        }
        if (categoriesTreeTableView.getSelectionModel().getSelectedItem() == null) {
            categoriesTreeTableView.getSelectionModel().select(root);
        }
    }
}
