package org.petrpan.ebookcatalog.controllers;

import javafx.scene.Node;

import java.util.HashMap;
import java.util.Map;

public class Context {
    private Map<Node, UpdateableController> nodeControllers = new HashMap<>();
    private Context() {};

    private static class SingletonHolder {
        public static final Context HOLDER_INSTANCE = new Context();
    }

    public static Context getInstance() {
        return SingletonHolder.HOLDER_INSTANCE;
    }

    public void setController(Node node, UpdateableController controller) {
        nodeControllers.put(node, controller);
    }

    public UpdateableController getController(Node node) {
        return nodeControllers.get(node);
    }
}
