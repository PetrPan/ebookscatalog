package org.petrpan.ebookcatalog.controllers;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.petrpan.ebookcatalog.model.BaseDirectoriesList;
import org.petrpan.ebookcatalog.model.File;

import java.time.format.DateTimeFormatter;

public class FileDirectoryEditController extends UpdateableController {
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
    public Button btnCancel;
    public TextField fileName;
    public TextField baseDirectory;
    public TextField DateOfChange;
    private File file;

    public void initFile(File file) {
        this.file = file;
        update();
    }

    public void onActionCancel(ActionEvent actionEvent) {
        Stage stage = (Stage) btnCancel.getScene().getWindow();
        stage.close();
    }

    @Override
    void update() {
        this.fileName.setText(file.getFilename());
        this.baseDirectory.setText(BaseDirectoriesList.getInstance().getRepresentation(file.getBaseDirectory()));
        this.DateOfChange.setText(file.getDateOfChange().format(formatter));
        Stage stage = (Stage) btnCancel.getScene().getWindow();
        stage.setTitle(file.getAlias());
    }
}
