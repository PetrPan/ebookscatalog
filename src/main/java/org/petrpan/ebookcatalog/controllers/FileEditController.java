package org.petrpan.ebookcatalog.controllers;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.petrpan.ebookcatalog.model.BaseDirectoriesList;
import org.petrpan.ebookcatalog.model.File;

import java.time.format.DateTimeFormatter;

public class FileEditController extends UpdateableController {
    public Button btnOK;
    public Button btnCancel;
    private File file;
    public TextField alias;
    public TextField yearOfPublishing;
    public TextField fileName;
    public TextField baseDirectory;
    public TextField size;
    public TextField extention;
    public TextField DateOfChange;
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

    public void initFile(File file) {
        this.file = file;
        update();
    }

    public void onActionSave(ActionEvent actionEvent) {
        boolean needToSave = false;
        if (!file.getAlias().equals(alias.getText())) {
            file.setAlias(alias.getText());
            needToSave = true;
        }
        if (file.getYearOfPublishing() != Integer.parseInt(yearOfPublishing.getText())) {
            file.setYearOfPublishing(Integer.parseInt(yearOfPublishing.getText()));
            needToSave = true;
        }
        if (needToSave) {
            file.saveToDB();
            if (onChangeCallback != null) onChangeCallback.changeNotification();
        }
        Stage stage = (Stage) btnOK.getScene().getWindow();
        stage.close();
    }

    public void onActionCancel(ActionEvent actionEvent) {
        Stage stage = (Stage) btnOK.getScene().getWindow();
        stage.close();
    }

    @Override
    void update() {
        this.alias.setText(file.getAlias());
        this.fileName.setText(file.getFilename());
        this.yearOfPublishing.setText(String.valueOf(file.getYearOfPublishing()));
        this.baseDirectory.setText(BaseDirectoriesList.getInstance().getRepresentation(file.getBaseDirectory()));
        this.size.setText(String.valueOf(file.getSize()));
        this.extention.setText(String.valueOf(file.getExtention()));
        this.DateOfChange.setText(file.getDateOfChange().format(formatter));
        Stage stage = (Stage) btnOK.getScene().getWindow();
        stage.setTitle(file.getAlias());
    }
}
