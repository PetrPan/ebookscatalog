package org.petrpan.ebookcatalog.controllers;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.petrpan.ebookcatalog.Main;
import org.petrpan.ebookcatalog.model.File;
import org.petrpan.ebookcatalog.model.FilesList;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class FilesController extends UpdateableController implements Initializable {
    public TreeTableView<File> filesTreeTableView;
    private final TreeItem<File> root = new TreeItem<>(new File(0, 0, "root", "root", 0, 0, 0, null, null, false));
    private final Map<Long, TreeItem<File>> indexOfTree = new LinkedHashMap<>();

    private FileEditController fileEditController;
    private Stage fileEditStage;

    private FileDirectoryEditController fileDirectoryEditController;
    private Stage fileDirectoryEditStage;

    private ContextMenu createFullContextMenu() {
        MenuItem changeMenuItem = new MenuItem("Изменить");
        changeMenuItem.setOnAction(actionEvent -> {
            var fileTreeItem = (TreeItem<File>) filesTreeTableView.getSelectionModel().getSelectedItem();
            if (fileTreeItem == null || fileTreeItem == root) return;
            if (fileTreeItem.getValue().isDirectory()) {
                fileDirectoryEditController.initFile(fileTreeItem.getValue());
                fileDirectoryEditStage.showAndWait();
            } else {
                fileEditController.initFile(fileTreeItem.getValue());
                fileEditStage.showAndWait();
            }
        });
        return new ContextMenu(changeMenuItem);
    }

    private ContextMenu createDeleteContextMenu() {
        MenuItem changeMenuItem = new MenuItem("Удалить");
        changeMenuItem.setOnAction(actionEvent -> {
            var fileTreeItem = (TreeItem<File>) filesTreeTableView.getSelectionModel().getSelectedItem();
            if (fileTreeItem == null || fileTreeItem == root || !fileTreeItem.getValue().isFileNotFound()) return;
            Alert alert = new Alert(Alert.AlertType.NONE);
            ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(Main.MAIN_ICON);
            alert.getDialogPane().getButtonTypes().setAll(ButtonType.OK, new ButtonType("Отмена", ButtonBar.ButtonData.CANCEL_CLOSE));
            alert.setTitle("Удаление");
            alert.setContentText("Удалить из каталога книгу?\n «" + fileTreeItem.getValue().getAlias() + "»");
            Optional<ButtonType> result = alert.showAndWait();
            result.ifPresent(buttonType -> {
                if (buttonType == ButtonType.OK) {
                    fileTreeItem.getParent().getChildren().remove(fileTreeItem);
                    filesTreeTableView.getSelectionModel().select(fileTreeItem.getParent());
                    fileTreeItem.getValue().deleteFromDB();
                    if (onChangeCallback != null) onChangeCallback.changeNotification();
                }
            });
        });
        return new ContextMenu(changeMenuItem);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Context.getInstance().setController(filesTreeTableView, this);
        initFileEditDialog();
        initFileDirectoryEditDialog();

        TreeTableColumn<File, String> fileAliasColumn = new TreeTableColumn<>("Название");
        fileAliasColumn.setCellValueFactory(p -> p.getValue().getValue().aliasProperty());
        fileAliasColumn.prefWidthProperty().bind(filesTreeTableView.widthProperty().multiply(0.8));

        TreeTableColumn<File, Number> fileYearOfPublishingColumn = new TreeTableColumn<>("Год издания");
        fileYearOfPublishingColumn.setCellValueFactory(p -> p.getValue().getValue().yearOfPublishingProperty());
        fileYearOfPublishingColumn.setCellFactory(p -> {
            TreeTableCell<File, Number> cell = new TreeTableCell<>() {
                @Override
                protected void updateItem(Number number, boolean empty) {
                    super.updateItem(number, empty);
                    setText(empty || number.equals(0) ? null : number.toString());
                }
            };
            cell.setAlignment(Pos.CENTER);
            return cell;
        });
        fileYearOfPublishingColumn.prefWidthProperty().bind(filesTreeTableView.widthProperty().multiply(0.19));

        var fullContextMenu = createFullContextMenu();
        var deleteContextMenu = createDeleteContextMenu();

        filesTreeTableView.setRowFactory(p -> new TreeTableRow<>() {
            @Override
            public void updateIndex(int i) {
                super.updateIndex(i);
                var treeItem = filesTreeTableView.getTreeItem(i);
                if (treeItem != null && treeItem.getValue().isFileNotFound()) {
                    setStyle("-fx-font-weight: bold; -fx-font-style: italic;");
                    setContextMenu(deleteContextMenu);
                } else if (treeItem != null && treeItem != root) {
                    setContextMenu(fullContextMenu);
                    setStyle("-fx-font-weight: normal; -fx-font-style: normal;");
                }
            }
        });

        filesTreeTableView.getColumns().addAll(List.of(fileAliasColumn, fileYearOfPublishingColumn));

        filesTreeTableView.setRoot(root);
        filesTreeTableView.setShowRoot(false);

        filesTreeTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        update();
    }

    private void initFileEditDialog() {
        FXMLLoader fileEditLoader = new FXMLLoader(getClass().getResource("/file_edit.fxml"));
        Scene scene;
        try {
            scene = new Scene(fileEditLoader.load());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        fileEditStage = new Stage();
        fileEditStage.setScene(scene);
        fileEditStage.getIcons().add(Main.MAIN_ICON);
        fileEditController = fileEditLoader.getController();
        fileEditController.setOnChangeCallback(() -> {
            if (this.onChangeCallback != null) onChangeCallback.changeNotification();
        });
    }

    private void initFileDirectoryEditDialog() {
        FXMLLoader fileEditLoader = new FXMLLoader(getClass().getResource("/files_directory_edit.fxml"));
        Scene scene;
        try {
            scene = new Scene(fileEditLoader.load());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        fileDirectoryEditStage = new Stage();
        fileDirectoryEditStage.setScene(scene);
        fileDirectoryEditStage.getIcons().add(Main.MAIN_ICON);
        fileDirectoryEditController = fileEditLoader.getController();
        fileDirectoryEditController.setOnChangeCallback(() -> {
            if (this.onChangeCallback != null) onChangeCallback.changeNotification();
        });
    }

    @Override
    void update() {
        var selectedItem = filesTreeTableView.getSelectionModel().getSelectedItem();
        File selectedFile = selectedItem == null ? null : selectedItem.getValue();
        root.getChildren().clear();

        indexOfTree.clear();
        indexOfTree.put(0L, root);

        for (File file : FilesList.getInstance().readFromDB()) {
            var fileTreeItem = new TreeItem<>(file);
            var parentTreeItem = indexOfTree.get(file.getParent());
            parentTreeItem.getChildren().add(fileTreeItem);
            indexOfTree.put(file.getId(), fileTreeItem);
            if (selectedFile != null && selectedFile.equals(file)) {
                while (parentTreeItem != null && !parentTreeItem.isExpanded()) {
                    parentTreeItem.setExpanded(true);
                    parentTreeItem = parentTreeItem.getParent();
                }
                filesTreeTableView.getSelectionModel().select(fileTreeItem);
            }
        }
        if (filesTreeTableView.getSelectionModel().getSelectedItem() == null) {
            filesTreeTableView.getSelectionModel().select(root);
        }
    }

    public void onMousePressed(MouseEvent mouseEvent) {
        if (mouseEvent.isPrimaryButtonDown() && mouseEvent.getClickCount() == 2) {
            var fileTreeItem = filesTreeTableView.getSelectionModel().getSelectedItem();
            if (fileTreeItem == null || fileTreeItem == root) return;
            // TODO открыть файл
        }
    }
}
