package org.petrpan.ebookcatalog.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    public AnchorPane leftAuthorsPane;
    public AnchorPane leftCategoriesPane;
    public AnchorPane leftBaseDirectoriesPane;
    public AnchorPane rightAuthorsPane;
    public AnchorPane rightCategoriesPane;
    public AnchorPane rightBaseDirectoriesPane;
    public AnchorPane leftFilesPane;
    public AnchorPane rightFilesPane;
    Stage primaryStage;

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        UpdateableController leftAuthorsController = Context.getInstance().getController(leftAuthorsPane.getChildren().get(0));
        UpdateableController leftCategoriesController = Context.getInstance().getController(leftCategoriesPane.getChildren().get(0));
        UpdateableController leftBaseDirectoryController = Context.getInstance().getController(leftBaseDirectoriesPane.getChildren().get(0));
        UpdateableController leftFilesController = Context.getInstance().getController(leftFilesPane.getChildren().get(0));

        UpdateableController rightAuthorsController = Context.getInstance().getController(rightAuthorsPane.getChildren().get(0));
        UpdateableController rightCategoriesController = Context.getInstance().getController(rightCategoriesPane.getChildren().get(0));
        UpdateableController rightBaseDirectoryController = Context.getInstance().getController(rightBaseDirectoriesPane.getChildren().get(0));
        UpdateableController rightFilesController = Context.getInstance().getController(rightFilesPane.getChildren().get(0));

        leftAuthorsController.setOnChangeCallback(rightAuthorsController::update);
        leftCategoriesController.setOnChangeCallback(rightCategoriesController::update);
        leftBaseDirectoryController.setOnChangeCallback(() -> {
            rightBaseDirectoryController.update();
            leftFilesController.update();
            rightFilesController.update();
        });
        leftFilesController.setOnChangeCallback(() -> {
            leftFilesController.update();
            rightFilesController.update();
        });

        rightAuthorsController.setOnChangeCallback(leftAuthorsController::update);
        rightCategoriesController.setOnChangeCallback(leftCategoriesController::update);
        rightBaseDirectoryController.setOnChangeCallback(() -> {
            leftBaseDirectoryController.update();
            leftFilesController.update();
            rightFilesController.update();
        });
        rightFilesController.setOnChangeCallback(() -> {
            leftFilesController.update();
            rightFilesController.update();
        });
    }

    interface Callback {
        void changeNotification();
    }

}
