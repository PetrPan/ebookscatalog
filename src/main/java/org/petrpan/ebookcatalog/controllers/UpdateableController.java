package org.petrpan.ebookcatalog.controllers;

public abstract class UpdateableController {
    protected MainController.Callback onChangeCallback;
    public void setOnChangeCallback(MainController.Callback onChangeCallback) {
        this.onChangeCallback = onChangeCallback;
    };
    abstract void update();
}
