package org.petrpan.ebookcatalog.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Author extends Entity {
    private StringProperty name;

    public Author() {}

    public Author(String name) {
        this.name = new SimpleStringProperty(name);
    }

    public Author(long id, String name) {
        this(name);
        this.id = id;
        this.isNew = false;
    }

    public void setName(String name) {
        this.name.setValue(name);
    }

    public String getName() {
        return name.getValue();
    }

    public StringProperty nameProperty() {
        return name;
    }

    @Override
    String tableName() {
        return "authors";
    }

    @Override
    String createQuery() {
        return "CREATE TABLE authors (id IDENTITY, name VARCHAR(256))";
    }

    @Override
    public void saveToDB(Connection connection) {
        String sqlQuery = isNew ? "INSERT INTO authors(name) VALUES(?)" :
                "UPDATE authors SET name = ?  WHERE id = ?";
        try (PreparedStatement query = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS)) {
            query.setString(1, name.getValue());
            if (!isNew) query.setLong(2, id);
            query.executeUpdate();
            if (isNew) {
                ResultSet rs = query.getGeneratedKeys();
                if (rs != null && rs.next()) {
                    id = rs.getLong(1);
                    isNew = false;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
