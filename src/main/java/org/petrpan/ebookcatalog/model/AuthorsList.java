package org.petrpan.ebookcatalog.model;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class AuthorsList extends EntitiesList<Author> {
    private AuthorsList() {
    }

    public static AuthorsList getInstance() {
        return SingletonHolder.HOLDER_INSTANCE;
    }

    @Override
    public List<Author> readFromDB(Connection connection, String fieldOfCondition, long valueOfCondition) {
        String sqlQuery = "SELECT id, name FROM authors ORDER BY name";
        return readFromDB(connection, sqlQuery, rs -> {
            try {
                return new Author(rs.getLong(1), rs.getString(2));
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        });
    }

    private static class SingletonHolder {
        public static final AuthorsList HOLDER_INSTANCE = new AuthorsList();
    }

}
