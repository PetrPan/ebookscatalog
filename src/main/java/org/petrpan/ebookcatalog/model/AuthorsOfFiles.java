package org.petrpan.ebookcatalog.model;

import java.sql.*;

public class AuthorsOfFiles extends Entity {
    private long author;
    private long file;

    public AuthorsOfFiles() {}

    public AuthorsOfFiles(long author, long file) {
        this.author = author;
        this.file = file;
    }

    public AuthorsOfFiles(long id, long author, long file) {
        this(author, file);
        this.id = id;
        this.file = file;
    }

    @Override
    String tableName() {
        return "authors_of_files";
    }

    @Override
    String createQuery() {
        return "CREATE TABLE authors_of_files (id IDENTITY, author BIGINT, file BIGINT)";
    }

    @Override
    public void saveToDB(Connection connection) {
        String sqlQuery = isNew ? "INSERT INTO caregories_of_files(author, file) VALUES(?, ?)" :
                "UPDATE caregories_of_files SET (author, file) =  (?, ?) WHERE id = ?";
        try (PreparedStatement query = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS)) {
            query.setLong(1, author);
            query.setLong(2, file);
            if (!isNew) query.setLong(3, id);
            query.executeUpdate();
            if (isNew) {
                ResultSet rs = query.getGeneratedKeys();
                if (rs != null && rs.next()) {
                    id = rs.getLong(1);
                    isNew = false;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
