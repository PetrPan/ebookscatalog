package org.petrpan.ebookcatalog.model;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaseDirectoriesList extends EntitiesList<BaseDirectory> {

    private Map<Long, String> baseDirectoryRepresentations = new HashMap<>();

    private BaseDirectoriesList() {
    }

    public static BaseDirectoriesList getInstance() {
        return BaseDirectoriesList.SingletonHolder.HOLDER_INSTANCE;
    }

    @Override
    public List<BaseDirectory> readFromDB(Connection connection, String fieldOfCondition, long valueOfCondition) {
        baseDirectoryRepresentations.clear();
        String sqlQuery = "SELECT id, path FROM base_directories ORDER BY path";
        return readFromDB(connection, sqlQuery, rs -> {
            try {
                var newBaseDirectory = new BaseDirectory(rs.getLong(1), rs.getString(2));
                baseDirectoryRepresentations.put(rs.getLong(1), rs.getString(2));
                return newBaseDirectory;
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        });
    }

    private static class SingletonHolder {
        public static final BaseDirectoriesList HOLDER_INSTANCE = new BaseDirectoriesList();
    }

    public String getRepresentation(long id) {
        return baseDirectoryRepresentations.get(id);
    }

}
