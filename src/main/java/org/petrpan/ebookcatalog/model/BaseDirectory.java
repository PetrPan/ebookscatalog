package org.petrpan.ebookcatalog.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.sql.*;

public class BaseDirectory extends Entity {
    private StringProperty path;

    public BaseDirectory() {}

    public BaseDirectory(String path) {
        this.path = new SimpleStringProperty(path);
    }

    public BaseDirectory(long id, String path) {
        this(path);
        this.id = id;
        this.isNew = false;
    }

    public void setPath(String path) {
        this.path.setValue(path);
    }

    public String getPath() {
        return path.getValue();
    }

    public StringProperty pathProperty() {
        return path;
    }

    @Override
    String tableName() {
        return "base_directories";
    }

    @Override
    String createQuery() {
        return "CREATE TABLE base_directories (id IDENTITY, path VARCHAR(1024))";
    }

    @Override
    public void saveToDB(Connection connection) {
        String sqlQuery = isNew ? "INSERT INTO base_directories(path) VALUES(?)" :
                "UPDATE base_directories SET path = ?  WHERE id = ?";
        try (PreparedStatement query = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS)) {
            query.setString(1, path.getValue());
            if (!isNew) query.setLong(2, id);
            query.executeUpdate();
            if (isNew) {
                ResultSet rs = query.getGeneratedKeys();
                if (rs != null && rs.next()) {
                    id = rs.getLong(1);
                    isNew = false;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
