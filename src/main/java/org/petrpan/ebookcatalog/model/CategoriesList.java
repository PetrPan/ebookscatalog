package org.petrpan.ebookcatalog.model;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class CategoriesList extends EntitiesList<Category> {
    private CategoriesList() {
    }

    public static CategoriesList getInstance() {
        return CategoriesList.SingletonHolder.HOLDER_INSTANCE;
    }

    @Override
    public List<Category> readFromDB(Connection connection, String fieldOfCondition, long valueOfCondition) {
        String sqlQuery = "SELECT id, parent, level, name FROM categories ORDER BY level, name";
        return readFromDB(connection, sqlQuery, rs -> {
            try {
                return new Category(rs.getLong(1), rs.getLong(2), rs.getInt(3), rs.getString(4));
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        });
    }

    private static class SingletonHolder {
        public static final CategoriesList HOLDER_INSTANCE = new CategoriesList();
    }

}
