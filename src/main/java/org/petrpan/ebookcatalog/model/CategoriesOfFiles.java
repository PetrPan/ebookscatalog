package org.petrpan.ebookcatalog.model;

import java.sql.*;

public class CategoriesOfFiles extends Entity {
    private long category;
    private long file;

    public CategoriesOfFiles() {}

    public CategoriesOfFiles(long category, long file) {
        this.category = category;
        this.file = file;
    }

    public CategoriesOfFiles(long id, long category, long file) {
        this(category, file);
        this.id = id;
        this.file = file;
    }

    @Override
    String tableName() {
        return "categories_of_files";
    }

    @Override
    String createQuery() {
        return "CREATE TABLE categories_of_files (id IDENTITY, category BIGINT, file BIGINT)";
    }

    @Override
    public void saveToDB(Connection connection) {
        String sqlQuery = isNew ? "INSERT INTO caregories_of_files(category, file) VALUES(?, ?)" :
                "UPDATE caregories_of_files SET (category, file) =  (?, ?) WHERE id = ?";
        try (PreparedStatement query = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS)) {
            query.setLong(1, category);
            query.setLong(2, file);
            if (!isNew) query.setLong(3, id);
            query.executeUpdate();
            if (isNew) {
                ResultSet rs = query.getGeneratedKeys();
                if (rs != null && rs.next()) {
                    id = rs.getLong(1);
                    isNew = false;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
