package org.petrpan.ebookcatalog.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.sql.*;

public class Category extends Entity {
    private StringProperty name;
    private int level;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Category() {
    }

    public Category(long parent, int level, String name) {
        this.parent = parent;
        this.level = level;
        this.name = new SimpleStringProperty(name);
    }

    public Category(long id, long parent, int level, String name) {
        this(parent, level, name);
        this.id = id;
        this.isNew = false;
    }

    public String getName() {
        return name.getValue();
    }

    public void setName(String name) {
        this.name.setValue(name);
    }

    public StringProperty nameProperty() {
        return name;
    }

    @Override
    String tableName() {
        return "categories";
    }

    @Override
    String createQuery() {
        return "CREATE TABLE categories (id IDENTITY, parent BIGINT, level INT, name VARCHAR(256))";
    }

    @Override
    public void saveToDB(Connection connection) {
        String sqlQuery = isNew ? "INSERT INTO categories(parent, level, name) VALUES(?, ?, ?)" :
                "UPDATE categories SET parent = ?, level = ?, name = ?  WHERE id = ?";
        try (PreparedStatement query = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS)) {
            query.setLong(1, parent);
            query.setInt(2, level);
            query.setString(3, name.getValue());
            if (!isNew) query.setLong(4, id);
            query.executeUpdate();
            if (isNew) {
                ResultSet rs = query.getGeneratedKeys();
                if (rs != null && rs.next()) {
                    id = rs.getLong(1);
                    isNew = false;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
