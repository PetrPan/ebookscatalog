package org.petrpan.ebookcatalog.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DBDriver {
    private List<Entity> entities;

    private static class SingletonHolder {
        public static final DBDriver HOLDER_INSTANCE = new DBDriver();
    }

    public static DBDriver getInstance() {
        return SingletonHolder.HOLDER_INSTANCE;
    }

    private void initDriver(Entity... entities) {
        this.entities = List.of(entities);
    }

    public void initDriver() {
        initDriver(new Author(), new CategoriesOfFiles(), new Category(), new BaseDirectory(), new File(), new Settings());
    }

    private DBDriver() {}

    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:h2:./h2/data", "sa", "sa");
            for (Entity entity : entities) {
                String tableName = entity.tableName().toUpperCase();
                ResultSet resultSet = connection.getMetaData().getTables("DATA", "PUBLIC", tableName, new String[]{"TABLE"});
                if (!resultSet.next()) {
                    try (Statement statement = connection.createStatement()) {
                        statement.execute(entity.createQuery());
                    }
                }
                resultSet.close();
            }
        } catch (SQLException e) {
            try {
                if (connection != null && !connection.isClosed()) connection.close();
            } catch (SQLException ex) { }
            throw new RuntimeException(e);
        }
        return connection;
    }

}
