package org.petrpan.ebookcatalog.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public abstract class EntitiesList<T extends Entity> {

    public List<T> readFromDB() {
        try (Connection connection = DBDriver.getInstance().getConnection()) {
            return readFromDB(connection);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<T> readFromDB(String fieldOfCondition, long valueOfCondition) {
        try (Connection connection = DBDriver.getInstance().getConnection()) {
            return readFromDB(connection, fieldOfCondition, valueOfCondition);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<T> readFromDB(Connection connection, String sqlQuery, Function<ResultSet, T> entityConstructor) {
        List<T> result = new ArrayList<>();
        try (PreparedStatement query = connection.prepareStatement(sqlQuery)) {
            ResultSet rs = query.executeQuery();
            while (rs.next()) {
                result.add(entityConstructor.apply(rs));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    public List<T> readFromDB(Connection connection) {
        return readFromDB(connection, null, 0);
    }

    public abstract List<T> readFromDB(Connection connection, String fieldOfCondition, long valueOfCondition);
}
