package org.petrpan.ebookcatalog.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public abstract class Entity {
    protected boolean isNew = true;
    protected long id = 0;
    protected long parent = 0;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getParent() {
        return parent;
    }

    public void setParent(long parent) {
        this.parent = parent;
    }

    public boolean isNew() {
        return isNew;
    }

    @Override
    public int hashCode() {
        return (int) id;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj.getClass() == this.getClass() && ((Entity) obj).id == this.id;
    }

    abstract String tableName();

    abstract String createQuery();

    public void saveToDB() {
        try (Connection connection = DBDriver.getInstance().getConnection()) {
            saveToDB(connection);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    abstract public void saveToDB(Connection connection);

    public void deleteFromDB() {
        try (Connection connection = DBDriver.getInstance().getConnection()) {
            deleteFromDB(connection);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void deleteFromDB(Connection connection) {
        if (isNew) return;
        String sqlQuery = "DELETE FROM " + tableName() + " WHERE id = ?";
        try (PreparedStatement query = connection.prepareStatement(sqlQuery)) {
            query.setLong(1, id);
            query.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
