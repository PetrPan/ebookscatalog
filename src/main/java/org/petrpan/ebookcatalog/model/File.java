package org.petrpan.ebookcatalog.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.sql.*;
import java.time.LocalDateTime;

public class File extends Entity {
    private StringProperty filename;
    private LocalDateTime dateOfChange;
    private long size;
    private String extention;
    private StringProperty alias;
    private IntegerProperty yearOfPublishing;
    private int level;
    private long baseDirectory;
    private boolean fileNotFound;
    private boolean isDirectory;

    public LocalDateTime getDateOfChange() {
        return dateOfChange;
    }

    public long getSize() {
        return size;
    }

    public String getExtention() {
        return extention;
    }

    public long getBaseDirectory() {
        return baseDirectory;
    }

    public File() {
    }

    public File(long parent, int level, String filename, String alias, int yearOfPublishing, long baseDirectory,
                long size, String extention, LocalDateTime dateOfChange, boolean fileNotFound) {
        this.parent = parent;
        this.level = level;
        this.filename = new SimpleStringProperty(filename);
        this.dateOfChange = dateOfChange;
        this.size = size;
        this.extention = extention;
        this.alias = new SimpleStringProperty(alias.isBlank() ? filename : alias);
        this.yearOfPublishing = new SimpleIntegerProperty(yearOfPublishing);
        this.baseDirectory = baseDirectory;
        this.fileNotFound = fileNotFound;
    }

    public File(long id, long parent, int level, String filename, String alias, int yearOfPublishing,
                long baseDirectory, long size, String extention, LocalDateTime dateOfChange, boolean fileNotFound) {
        this(parent, level, filename, alias, yearOfPublishing, baseDirectory, size, extention, dateOfChange, fileNotFound);
        this.id = id;
        this.isNew = false;
    }

    public boolean isDirectory() {
        return isDirectory;
    }

    public void setDirectory(boolean directory) {
        isDirectory = directory;
    }

    public boolean isFileNotFound() {
        return fileNotFound;
    }

    public void setFileNotFound(boolean fileNotFound) {
        this.fileNotFound = fileNotFound;
    }

    public String getFilename() {
        return filename.getValue();
    }

    public void setFilename(String filename) {
        this.filename.setValue(filename);
    }

    public StringProperty filenameProperty() {
        return filename;
    }

    public String getAlias() {
        return alias.getValue();
    }

    public void setAlias(String alias) {
        this.alias.setValue(alias.trim());
    }

    public StringProperty aliasProperty() {
        return alias;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    String tableName() {
        return "files";
    }

    @Override
    String createQuery() {
        return "CREATE TABLE files (id IDENTITY, parent BIGINT, level INT, filename VARCHAR(256), alias VARCHAR(256), year_of_publishing INT, " +
                "base_directory BIGINT, size BIGINT, extention VARCHAR(32), date_of_change TIMESTAMP, file_not_found BOOLEAN, is_directory BOOLEAN)";
    }

    public int getYearOfPublishing() {
        return yearOfPublishing.get();
    }

    public IntegerProperty yearOfPublishingProperty() {
        return yearOfPublishing;
    }

    public void setYearOfPublishing(int yearOfPublishing) {
        this.yearOfPublishing.set(yearOfPublishing);
    }

    @Override
    public void saveToDB(Connection connection) {
        String sqlQuery = isNew ?
                "INSERT INTO " +
                        "files(parent, level, filename, alias, year_of_publishing, base_directory, size, extention, date_of_change, file_not_found, is_directory) " +
                        "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" :
                "UPDATE files SET (parent, level, filename, alias, year_of_publishing, base_directory, size, extention, date_of_change, file_not_found, is_directory) " +
                        "=  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) WHERE id = ?";
        try (PreparedStatement query = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS)) {
            query.setLong(1, parent);
            query.setLong(2, level);
            query.setString(3, filename.getValue());
            query.setString(4, alias.getValue());
            query.setInt(5, yearOfPublishing.getValue());
            query.setLong(6, baseDirectory);
            query.setLong(7, size);
            query.setString(8, extention);
            query.setTimestamp(9, Timestamp.valueOf(dateOfChange));
            query.setBoolean(10, fileNotFound);
            query.setBoolean(11, isDirectory);
            if (!isNew) query.setLong(12, id);
            query.executeUpdate();
            if (isNew) {
                ResultSet rs = query.getGeneratedKeys();
                if (rs != null && rs.next()) {
                    id = rs.getLong(1);
                    isNew = false;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
