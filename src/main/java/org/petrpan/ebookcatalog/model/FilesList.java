package org.petrpan.ebookcatalog.model;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class FilesList extends EntitiesList<File> {

    private FilesList() {
    }

    public static FilesList getInstance() {
        return FilesList.SingletonHolder.HOLDER_INSTANCE;
    }

    public void readFromDisc(BaseDirectory directory) throws IOException {
        Path basePath = Paths.get(directory.getPath());
        if (!Files.exists(basePath)) return;
        FileVisitor fileVisitor = new FileVisitor("pdf", "djvu", "djv");
        Files.walkFileTree(basePath, fileVisitor);
        Map<Path, File> filesFromDisk = new LinkedHashMap<>(fileVisitor.getPathBooleanMap().size());
        Map<Path, File> filesFromDB = FilesList.getInstance().readFromDBToMap(directory);
        try (Connection connection = DBDriver.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            for (Path path : fileVisitor.getPathBooleanMap().keySet()) {
                if (path.equals(basePath)) continue;
                if (filesFromDB.containsKey(path)) {
                    filesFromDisk.put(path, filesFromDB.get(path));
                    var fileFromDB = filesFromDB.get(path);
                    if (fileFromDB.isFileNotFound()) {
                        fileFromDB.setFileNotFound(false);
                        fileFromDB.saveToDB(connection);
                    }
                    filesFromDB.remove(path);
                } else {
                    Path parentPath = path.getParent();
                    long parentId = 0;
                    int level = 1;
                    if (parentPath != null && !parentPath.equals(basePath)) {
                        File parent = filesFromDisk.get(parentPath);
                        parentId = parent.getId();
                        level = parent.getLevel() + 1;
                    }
                    java.io.File ioFile = path.toFile();
                    String fileName = ioFile.getName();
                    String extension;
                    if (ioFile.isDirectory()) {
                        extension = "";
                    } else {
                        int dotIndex = fileName.lastIndexOf(".");
                        extension = dotIndex == -1 ? "" : fileName.substring(dotIndex + 1);
                        if (extension.length() > 64) extension = "";
                    }
                    File newFile = new File(parentId, level, fileName, fileName, 0, directory.getId(), ioFile.length(),
                            extension, LocalDateTime.ofEpochSecond(ioFile.lastModified() / 1000, 0, ZoneOffset.UTC),
                            false);
                    newFile.setDirectory(ioFile.isDirectory());
                    newFile.saveToDB(connection);
                    filesFromDisk.put(path, newFile);
                }
            }
            for (Map.Entry<Path, File> pathFileEntry : filesFromDB.entrySet()) {
                var fileFromDB = pathFileEntry.getValue();
                fileFromDB.setFileNotFound(true);
                fileFromDB.saveToDB(connection);
            }
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<File> readFromDB(Connection connection, String fieldOfCondition, long valueOfCondition) {
        String sqlQuery = "SELECT id, parent, level, filename, alias, year_of_publishing, base_directory, size, extention, " +
                "date_of_change, file_not_found, is_directory FROM files";
        if (fieldOfCondition != null) sqlQuery += "\nWHERE base_directory = " + valueOfCondition;
        sqlQuery += "\nORDER BY level, alias";
        return readFromDB(connection, sqlQuery, rs -> {
            try {
                var newFile = new File(rs.getLong(1), rs.getLong(2), rs.getInt(3), rs.getString(4),
                        rs.getString(5), rs.getInt(6), rs.getLong(7), rs.getLong(8),
                        rs.getString(9), rs.getTimestamp(10).toLocalDateTime(), rs.getBoolean(11));
                newFile.setDirectory(rs.getBoolean(12));
                return newFile;
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        });
    }

    public Map<Path, File> readFromDBToMap(BaseDirectory directory) {
        String sqlQuery = "SELECT id, parent, level, filename, alias, year_of_publishing, base_directory, size, extention, " +
                "date_of_change, file_not_found FROM files\n" +
                "WHERE base_directory = " + directory.getId() + " ORDER BY level, alias";
        Map<Path, File> result = new LinkedHashMap<>();
        Map<Long, Path> indexOfPaths = new HashMap<>();
        indexOfPaths.put(0L, Paths.get(directory.getPath()));
        try (Connection connection = DBDriver.getInstance().getConnection();
             PreparedStatement query = connection.prepareStatement(sqlQuery)) {
            ResultSet rs = query.executeQuery();
            while (rs.next()) {
                var newFile = new File(rs.getLong(1), rs.getLong(2), rs.getInt(3),
                        rs.getString(4), rs.getString(5), rs.getInt(6), rs.getLong(7),
                        rs.getLong(8), rs.getString(9), rs.getTimestamp(10).toLocalDateTime(),
                        rs.getBoolean(11));
                Path path = indexOfPaths.get(newFile.getParent()).resolve(newFile.getFilename());
                indexOfPaths.put(newFile.getId(), path);
                result.put(path, newFile);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private static class SingletonHolder {
        public static final FilesList HOLDER_INSTANCE = new FilesList();
    }

    private static class FileVisitor extends SimpleFileVisitor<Path> {
        private final List<String> extensions;
        private final Map<Path, Boolean> pathBooleanMap = new LinkedHashMap<>();

        public FileVisitor(String... extensions) {
            super();
            this.extensions = List.of(extensions);
        }

        public Map<Path, Boolean> getPathBooleanMap() {
            return pathBooleanMap;
        }

        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
            pathBooleanMap.put(dir, false);
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
            int dotIndex = file.toString().lastIndexOf(".");
            String extension = dotIndex == -1 ? "" : file.toString().substring(dotIndex + 1);
            if (extensions.contains(extension)) {
                pathBooleanMap.put(file, true);
                var parent = file.getParent();
                while (parent != null) {
                    if (pathBooleanMap.get(parent) == null || pathBooleanMap.get(parent)) break;
                    pathBooleanMap.put(parent, true);
                    parent = parent.getParent();
                }
            }
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
            if (!pathBooleanMap.get(dir)) pathBooleanMap.remove(dir);
            return FileVisitResult.CONTINUE;
        }
    }
}
