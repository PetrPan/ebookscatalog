package org.petrpan.ebookcatalog.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Settings extends Entity {

    public Settings() {};

    public static String getValue(String key, Connection connection) {
        String value = null;
        String sqlQuery = "SELECT value FROM settings WHERE key = ?";
        try (PreparedStatement query = connection.prepareStatement(sqlQuery)) {
            query.setString(1, key);
            ResultSet rs = query.executeQuery();
            if (rs.next()) {
                value = rs.getString(1);
            }
            rs.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return value;
    }

    public void setValue(String key, String value, Connection connection) {
        String sqlQuery = getValue(key, connection) == null ?
                "INSERT INTO settings(key, value) VALUES(?, ?)" :
                "UPDATE settings SET value = ? WHERE key = ?";
        try (PreparedStatement query = connection.prepareStatement(sqlQuery)) {
            query.setString(1, key);
            query.setString(2, value);
            query.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    String tableName() {
        return "settings";
    }

    @Override
    String createQuery() {
        return "CREATE TABLE settings (key VARCHAR(32), value VARCHAR(1024), PRIMARY KEY(key));\n";
    }

    @Override
    public void saveToDB(Connection connection) {

    }
}
